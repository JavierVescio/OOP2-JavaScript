<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sistema Franc�s - Cliente</title>
<script src="js/jquery-3.2.1.js"></script>
	<script type="text/javascript">
	
				$(document).ready(function (){
					$('#consultar').click(function (){
						var dni = $('#dni').val();
						$.ajax({
							method: "POST",
							url: "MostrarClienteJSP", // no sera /MostrarClienteJSP
							data: {dni: dni},
							async: false,
						}).done(function (data){
							$("#responsecliente").html(data);
						})
					});
				});

	</script>
</head>
<body>
	<%@ include file="/cabecera.jsp"%>
	<div class="jumbotron">
		<div class="container">
			<h1>B�squeda de clientes</h1>
			<form class="navbar-form navbar-right">
				<div class="form-group">
					<label for="dni">DNI:</label> 
					<input id="dni" name="dni">
				</div>
				<input id="consultar" type="button" class="btn btn-success"
					value="Consultar datos de cliente" />
			</form>
		</div>
		<div class="container">
			<div id="responsecliente"></div>
		</div>
	</div>
</body>
</html>

