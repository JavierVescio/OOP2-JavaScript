package negocio;
import dao.PrestamoDao;

import java.util.GregorianCalendar;
import java.util.List;

import datos.Cliente;
import datos.Prestamo;

public class PrestamoABM {
	private PrestamoDao dao = new PrestamoDao();
	
	public Prestamo traerPrestamo(long idPrestamo) throws Exception {
		//Implementar: si el no existe el prestamo lanzar la excepcion
		Prestamo p = dao.traerPrestamo(idPrestamo);
		if (p == null){
			throw new Exception("El prestamo no existe");
		}
		else{
			return p;
		}
	}
	
	public Prestamo traerPrestamoYCuotas(long idPrestamo) throws Exception {
		Prestamo p = dao.traerPrestamoYCuotas(idPrestamo);
		if (p == null){
			throw new Exception("El prestamo no existe");
		}
		else{
			return p;
		}
	}
	
	public List<Prestamo> traerPrestamo(Cliente c) {
		return dao.traerPrestamo(c);
	}
	
	public void agregar(GregorianCalendar fecha, double monto, double interes, int cantCuotas, Cliente cliente) {
		//Hecho por Javi ;) ;)
		Prestamo prestamo = new Prestamo(fecha,monto,interes,cantCuotas,cliente);
		
		dao.agregar(prestamo);
	}
	
	public void modificar(Prestamo prestamo) throws Exception {
		//Hecho por Javi ;) ;)
		if (dao.traerPrestamo(prestamo.getIdPrestamo()) == null)
			dao.actualizar(prestamo);
		else{
			throw new Exception("El idPrestamo ya existe!");
		}
	}
	
}
