package test;

import negocio.PrestamoABM;
import datos.Prestamo;
import datos.Cuota;

public class TestTraerPrestamoYCuotas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long idPrestamo = 3;
		PrestamoABM prestamoAbm = new PrestamoABM();
		try {
			Prestamo p = prestamoAbm.traerPrestamoYCuotas(idPrestamo);
			
			System.out.println("\n---> Traer prestamo y cuotas idPrestamo="+idPrestamo);
			System.out.println("\n"+p);

			//Implementar si no tiene cuotas otorgados imprima el mensaje
			boolean hay_cuotas = false;
			for (Cuota c: p.getCuotas()){
				System.out.println("\n"+c);
				hay_cuotas = true;
			}
			if (!hay_cuotas) {
				System.out.println("No hay cuotas!");
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

}
