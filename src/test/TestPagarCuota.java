package test;

import java.util.GregorianCalendar;

import datos.Cuota;
import negocio.CuotaABM;

public class TestPagarCuota {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int idCuota = 6;

		CuotaABM cuotaABM = new CuotaABM();

		try {
			Cuota cuota = cuotaABM.traerCuota(idCuota);

			if (!cuota.isCancelada()) {
				cuota.setCancelada(true);
				cuota.setFechaDePago(new GregorianCalendar());
				//if (cuota.getFechaDePago() > cuota.getFechaVencimiento())
				cuota.setPunitorios(0.02);

				cuotaABM.modificar(cuota);
			}

		}catch(Exception e){
			System.out.println(e.getMessage());
		}


	}

}
