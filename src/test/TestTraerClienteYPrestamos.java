package test;
import negocio.ClienteABM;
import datos.Cliente;
import datos.Prestamo;

public class TestTraerClienteYPrestamos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long idCliente = 3;
		ClienteABM cliAbm = new ClienteABM();
		try {
			Cliente c = cliAbm.traerClienteYPrestamos(idCliente);
			System.out.println("\n---> Traer cliente y prestamos idCliente="+idCliente);
			System.out.println("\n"+c);

			//Implementar si no tiene prestamos otorgados imprima el mensaje
			boolean hay_prestamos = false;
			for (Prestamo p: c.getPrestamos()){
				System.out.println("\n"+p);
				hay_prestamos = true;
			}
			if (!hay_prestamos) {
				System.out.println("No hay prestamos!");
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
}
