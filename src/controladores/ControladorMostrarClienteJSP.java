package controladores;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import negocio.ClienteABM;
import negocio.PrestamoABM;
import datos.Cliente;
import datos.Prestamo;

public class ControladorMostrarClienteJSP extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		procesarPeticion(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		procesarPeticion(request,response);
	}
	
	private void procesarPeticion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("puto el que lee");
		response.setContentType("text/html;charset=UTF-8");
		try {
			ClienteABM clienteabm = new ClienteABM();
			int dni= Integer.parseInt(request.getParameter("dni"));
			Cliente cliente = clienteabm.traerCliente(dni);
			//PrestamoABM prestamoAbm = new PrestamoABM();
			//List<Prestamo> lstPrestamos = prestamoAbm.traerPrestamo(cliente);
			
			request.setAttribute("cliente",cliente);
			//request.setAttribute("lstPrestamo", lstPrestamos);
			request.getRequestDispatcher("/ajaxvistacliente.jsp").forward(request, response);
		}
		catch(Exception e){
			response.sendError(500,"El DNI ingresado no existe en la base de datos.");
		}
	}
}
